library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.numeric_std.ALL;

entity pr_1 is Port
( clk : in  STD_LOGIC;
 rst : in  STD_LOGIC;
 LED :  out STD_LOGIC_VECTOR(7 DOWNTO 0);
 SevenSegment     : out std_logic_vector (7 downto 0);
 Enable           : out std_logic_vector(2 downto 0));
end pr_1;

architecture Behavioral of pr_1 is
-- deklaracja KCPSM3
component kcpsm3 Port
( address : out std_logic_vector(9 downto 0);
 instruction : in std_logic_vector(17 downto 0);
 port_id : out std_logic_vector(7 downto 0);
 write_strobe : out std_logic;
 out_port : out std_logic_vector(7 downto 0);
 read_strobe : out std_logic;  
 in_port : in std_logic_vector(7 downto 0);
 interrupt : in std_logic;
 interrupt_ack : out std_logic;
 reset : in std_logic;
 clk : in std_logic);
end component;
-- deklaracja pamieci programu ROM
component prog_1 Port
( address : in std_logic_vector(9 downto 0);
 instruction : out std_logic_vector(17 downto 0);
 clk : in std_logic);
end component;
-- Sygnaly uzyte do polaczenia KCPSM3 z pamiecia programu ROM
signal address : std_logic_vector(9 downto 0);
signal instruction : std_logic_vector(17 downto 0);
signal port_id : std_logic_vector(7 downto 0);
signal out_port : std_logic_vector(7 downto 0);
signal in_port : std_logic_vector(7 downto 0);
signal write_strobe    : std_logic;
signal read_strobe     : std_logic;
signal interrupt       : std_logic;
signal interrupt_ack   : std_logic;
signal main_reset      : std_logic;
-- Sygnaly dodatkowe
signal clk_p       : std_logic;
signal CLK_licznik : integer range 0 to 3199;
signal mryganie : STD_LOGIC_VECTOR(7 DOWNTO 0) := "00000000";

begin
-- KCPSM3 and the program memory 
 processor: kcpsm3
port map(      address => address,
              instruction => instruction,
                  port_id => port_id,
             write_strobe => write_strobe,
                 out_port => out_port,
              read_strobe => read_strobe,
                  in_port => in_port,
                interrupt => interrupt,
            interrupt_ack => interrupt_ack,
                    reset => main_reset,
                      clk => clk_p);
 program_rom: prog_1
 
port map(      address => address,
              instruction => instruction,
                      clk => clk_p);

 interrupt <= interrupt_ack;
 main_reset <= not rst;
 
 LED_ustaw: process (clk_p, rst)
variable SevenSegment_o       : std_logic_vector (7 downto 0) := (others => '0');
variable En                   : std_logic_vector(2 downto 0)  := B"110";
variable bcd                  : integer := 0;
variable bcd_1                : integer := 0;
variable bcd_2                : integer := 0;
variable bcd_3                : integer := 0;
 begin
   if (rst = '0') then
--     mryganie(0) <= '0';
   elsif clk_p'event and clk_p='1' then
     if ((port_id="00000000") and (write_strobe='1')) then
       mryganie <= out_port;
     end if;
   end if;
	
if rising_edge(clk_p) then
   
	bcd_2 := conv_integer(mryganie);
	bcd_1 := bcd_2;

   if (mryganie > 199) then
	    bcd_3 := 2;
		 bcd_2 := bcd_2 - 200;
		 bcd_1 := bcd_1 - 200;
   elsif (mryganie > 99) then
	    bcd_3 := 1;
		 bcd_2 := bcd_2 - 100;
		 bcd_1 := bcd_1 - 100;
	else
	    bcd_3 := 0;
	end if;
	
	if (bcd_2 < 10) then
	    bcd_2 := 0;
	elsif (bcd_2 > 9 and bcd_2 < 20) then
	    bcd_2 := 1;
		 bcd_1 := bcd_1 - 10;
   elsif (bcd_2 > 19 and bcd_2 < 30) then
	    bcd_2 := 2;
		 bcd_1 := bcd_1 - 20;
   elsif (bcd_2 > 29 and bcd_2 < 40) then
	    bcd_2 := 3;
		 bcd_1 := bcd_1 - 30;
   elsif (bcd_2 > 39 and bcd_2 < 50) then
	    bcd_2 := 4;
		 bcd_1 := bcd_1 - 40;
   elsif (bcd_2 > 49 and bcd_2 < 60) then
	    bcd_2 := 5;
		 bcd_1 := bcd_1 - 50;
   elsif (bcd_2 > 59 and bcd_2 < 70) then
	    bcd_2 := 6;
		 bcd_1 := bcd_1 - 60;
   elsif (bcd_2 > 69 and bcd_2 < 80) then
	    bcd_2 := 7;
		 bcd_1 := bcd_1 - 70;
   elsif (bcd_2 > 79 and bcd_2 < 90) then
	    bcd_2 := 8;
		 bcd_1 := bcd_1 - 80;
   elsif (bcd_2 > 89 and bcd_2 < 100) then
	    bcd_2 := 9;
		 bcd_1 := bcd_1 - 90;
	end if;

	if (En = B"110") then
		bcd := bcd_1;
	elsif (En = B"101") then
		bcd := bcd_2;
	else
		bcd := bcd_3;
	end if;
    case bcd is
	     when 0      => SevenSegment_o := B"00000010";
		  when 1      => SevenSegment_o := B"10011110";
		  when 2      => SevenSegment_o := B"00100100";
		  when 3      => SevenSegment_o := B"00001100";
		  when 4      => SevenSegment_o := B"10011000";
		  when 5      => SevenSegment_o := B"01001000";
		  when 6      => SevenSegment_o := B"01000000";
		  when 7      => SevenSegment_o := B"00011110";
		  when 8      => SevenSegment_o := B"00000000";
		  when 9      => SevenSegment_o := B"00011000";
		  when others => SevenSegment_o := B"11111111";
    end case;
	 Enable       <= En;
	 LED <= mryganie;
    SevenSegment <= SevenSegment_o;
	 En := En(1 downto 0) & En(2);
end if;

 end process;

 CLK_ustaw: process (clk, rst)
 begin
   if (rst = '0') then
     CLK_licznik <= 0;
     clk_p <= '0';
   elsif clk'event and clk='1' then
     if (CLK_licznik >= 3199) then
       CLK_licznik <= 0;
       clk_p <= not clk_p;
     else
       CLK_licznik <= CLK_licznik + 1;
     end if;
   end if;
 end process;
end Behavioral;